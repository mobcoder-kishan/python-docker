Pre-requisites-

	Docker should be installed in system.
	

1. Place the docker files with project directory and requirements.txt file.(As shared with this dummy project)

2. Execute the below docker command to run the project-
	docker-compose up --build -d

3. The docker will take time to setup the project.

4. To see the docker containers are up, execute below command-
	docker ps
	
    output-
	
	CONTAINER ID   IMAGE              COMMAND                  CREATED          STATUS         PORTS                                                      NAMES
	97c036224ff4   djangodocker_web   "python manage.py ru…"   4 minutes ago    Up 4 minutes   0.0.0.0:8000->8000/tcp, :::8000->8000/tcp                  web
	6e6e05964bdd   mongo              "docker-entrypoint.s…"   7 minutes ago    Up 4 minutes   27017/tcp, 0.0.0.0:27018->27018/tcp, :::27018->27018/tcp   mongo_database
	ef983bc2ee62   redis              "docker-entrypoint.s…"   26 minutes ago   Up 4 minutes   6379/tcp, 0.0.0.0:6380->6380/tcp, :::6380->6380/tcp        redis


5. To kill the docker container, run below command-
	docker kill <container_name>
	
	example - 
		docker kill web
		docker kill mongo_database
		
	container name are available in last column of step-4 output

6. To check the logs, execute below commands-
	docker logs <container_name>
	
	example -
		docker logs web
		docker logs mongo_database

7. To enter in specific container, follow below command-
	docker exec -it <container_id> /bin/bash
	
	example - 
		docker exec -it 97c036224ff4 /bin/bash
	
	container_id is available in first column of step-4 output
	
8. To restart, the docker, follow below command-
	docker-compose restart
	
		
Note- Please change the port in docker-compose.yml file, if required.
