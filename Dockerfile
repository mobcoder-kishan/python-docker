FROM python:3

USER root

WORKDIR /app

COPY . /app/

RUN python -m pip install --upgrade pip

RUN pip install -r requirements.txt

WORKDIR /app/TestProject

